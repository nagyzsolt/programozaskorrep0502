#include <stdio.h>
#include <stdlib.h>
int elsofeladat(char* string);
int masodikfeladat(int* tombPtr);
char harmadikfeladat(char* string, int stringLength);
char negyedikfeladat(int* tomb1, int* tomb2);
int main()
{
    char tomb1[]="Az egyik ZH egy hetenat mindenkinek sikerult.";
    sizeof(tomb1)/sizeof(tomb1[0]);
    printf("\nElso Feladat:%d", elsofeladat(tomb1));

    int tomb2[20] = {-685, 590, 670, 72, 101, 30,-200,
                     -605, 415,710,42, 68,-1, 13,366,12,
                     -1222, 670, 591,-73
                    };
    int b = masodikfeladat(tomb2);
    printf("\nMasodik Feladat:%d", b);

    char tomb3[]="A ZH az elmult hetenrossz lett!";
    char c = harmadikfeladat(tomb3, sizeof(tomb3)/sizeof(tomb3[0]));
    printf("\nHarmadik Feladat:%d", c);
    printf("\n%s", tomb3);

    int tomb4[] = {67,-685, 590, 670, 72, 101, 30,-200, -605, -122, 415,710,42, 68,-1, 13,366,12,672, 591,33, 151, 37};
    int tomb5[] = {-590, -122,672, 12,42, 392, 33,-225, 607, 455,710,33,1, 12, -135, 67, 72,670, 13, 366, 867, -200};
    int d = negyedikfeladat(tomb4, tomb5);
    printf("\nNegyedik feladat:%d", d);
    return 0;
}

char negyedikfeladat(int* tomb1, int* tomb2){
    int i, j;
    char darab=0;
    for(i=0;i<24;i++){
        for(j=0;j<24;j++){
            if(*(tomb1+i) == *(tomb2+j)){
               darab++;
            }
        }
    }
    return darab;
}

char harmadikfeladat(char* string, int stringLength)
{
    char darab=0;
    char i=0;
    char indexer=0;
    for(i=0; i<stringLength; i++)
    {
        if((*(string+i) == 'a') || (*(string+i) == 'e')
        || (*(string+i) == 'i') || (*(string+i) == 'o')
        || (*(string+i) == 'u'))
        {
            *(string+i)= 0;
            darab++;
        }
        if((*(string+i) == 'A') || (*(string+i) == 'E')
        || (*(string+i) == 'I') || (*(string+i) == 'O')
        || (*(string+i) == 'U'))
        {
            *(string+i)= 0;
            darab++;
        }
    }
    for(i=0; i<stringLength; i++){
        if(*(string+i)){
            *(string+indexer) = *(string+i);
            indexer++;
        }
    }
    for(;indexer<stringLength;indexer++){
        *(string+indexer) = 0;
    }
    return darab;
}

int masodikfeladat(int* tombPtr)
{
    int winner=*tombPtr;
    char i=0;
    for(i=1; i<20; i++)
    {
        if(winner < *(tombPtr+i))
        {
            winner= *(tombPtr+i);
        }
    }
    return winner;
}

int elsofeladat(char* string)
{
    int darab=0;
    unsigned char i=0;
    for(i=0; *(string+i); i++)
    {
        if((*(string+i) == 'e') || (*(string+i)== 'n'))
        {
            darab++;
        }
    }
    return darab;
}
